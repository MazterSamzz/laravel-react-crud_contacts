<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Its different from the tutorial because I tried to follow the standart function of Controller
Route::get('contacts', 'ContactsController@index');
Route::post('contact/store', 'ContactsController@store');
Route::get('contact/{id}/edit', 'ContactsController@edit');
Route::put('contact/{id}/update', 'ContactsController@update');
Route::delete('contact/{id}/delete', 'ContactsController@destroy');