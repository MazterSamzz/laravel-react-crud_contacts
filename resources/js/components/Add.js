import React, {Component} from 'react';
import { post } from 'jquery';
import ReactDOM from 'react-dom';
import axios from 'axios';

class Add extends Component {

    constructor(props){
        super(props);
        this.state = {
            name: '',
            tel: ''
        }
        this.handleNameInputChange = this.handleNameInputChange.bind(this);
        this.handleTelInputChange = this.handleTelInputChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
    }

    handleNameInputChange(event){
        this.setState({
            name : event.target.value
        })
    }
    handleTelInputChange(event){
        this.setState({
            tel : event.target.value
        })
    }

    handleFormSubmit(event){
        event.preventDefault();
        axios/post('/api/contact/store', {
            name: this.state.name,
            tel: this.state.tel
        }).then(response => {
            this.setState({
                name: '',
                tel: ''
            })
            this.props.history.push('/');
        }).catch(err => console.log(err));
    }

    render(){
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-header">Add Contact</div>
    
                            <div className="card-body">
                                <form onSubmit={this.handleFormSubmit}>
                                    <div className="form-group">
                                        <input type="text" className="form-control" id="email" placeholder="Enter Name"
                                                required
                                                onChange={this.handleNameInputChange}
                                                value={this.state.name}
                                        />
                                    </div>
                                    
                                    <div className="form-group">
                                        <input type="text" className="form-control" id="phone" placeholder="Phone"
                                                required
                                                onChange={this.handleTelInputChange}
                                                value={this.state.tel}
                                        />
                                    </div>
    
                                    <button type="submit" className="btn btn-primary">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Add;